from keras.models import Model
from keras.layers import Input
from keras.layers import LSTM, GRU
from numpy import array
import keras
k_init = keras.initializers.Constant(value=0.1)
b_init = keras.initializers.Constant(value=0)
r_init = keras.initializers.Constant(value=0.1)
# LSTM units
units = 2

# define model
inputs1 = Input(shape=(3, 2))
lstm1 = LSTM(units, return_sequences=True, kernel_initializer=k_init, bias_initializer=b_init, recurrent_initializer=r_init)(inputs1)
model = Model(inputs=inputs1, outputs=lstm1)
# define input data
data = array([0.1, 0.2, 0.3, 0.1, 0.2, 0.3]).reshape((1,3,2))
# print(data)
# make and show prediction
output = model.predict(data)
# print(output, output.shape)
#(#Samples, #Time steps, #LSTM units).

# define model
inputs1 = Input(shape=(2, 3))
lstm1, state_h, state_c = LSTM(units, return_state=True, return_sequences=False, kernel_initializer=k_init, bias_initializer=b_init, recurrent_initializer=r_init)(inputs1)
model = Model(inputs=inputs1, outputs=[lstm1, state_h, state_c])
# define input data

data = array([0.1, 0.2, 0.3, 0.1, 0.2, 0.3]).reshape((1,2,3))
# print('data')
# print(data)
# # make and show prediction
# print('output ')
# output = model.predict(data)
# print(output)
# for a in output:
#     print('a')
#     print(a)
#     print('a.shape')
#     print(a.shape)

#3 output cua last step. (#Samples, #LSTM units).
# define model
inputs1 = Input(shape=(3, 2))
lstm1, state_h, state_c = LSTM(units, return_sequences=True, return_state=True, kernel_initializer=k_init, bias_initializer=b_init, recurrent_initializer=r_init)(inputs1)
model = Model(inputs=inputs1, outputs=[lstm1, state_h, state_c])
# define input data
data = array([0.1, 0.2, 0.3, 0.1, 0.2, 0.3]).reshape((1,3,2))
# print('data',data)

# make and show prediction
# print('output ')
# output = model.predict(data)
# print(output)
# #(h<1...T>, h<T>, c<T>).
# for a in output:
#     print('a')
#     print(a)
#     print('a.shape')
#     print(a.shape)

#if we replace LSTM with GRU the output will have only two components. (h<1...T>, c<T>) since in GRU h<T>=c<T>.


inputs1 = Input(shape=(3, 2))
sample_output, sample_hidden = GRU(units, return_sequences=True, return_state=True, kernel_initializer=k_init, bias_initializer=b_init, recurrent_initializer=r_init)(inputs1)
model = Model(inputs=inputs1, outputs=[sample_output, sample_hidden])
# define input data
data = array([0.1, 0.2, 0.3, 0.1, 0.2, 0.3]).reshape((1,3,2))
print('data',data)

# make and show prediction
print('output ')
output = model.predict(data)
print(output)
#(h<1...T>, h<T>)
for a in output:
    print('a')
    print(a)
    print('a.shape')
    print(a.shape)
